document.addEventListener("DOMContentLoaded", function () {
    window.log("DOM content loaded");
});

function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    let c = f1[0].value * f2[0].value;
    r.innerHTML = (
        c.toString().match(/^[0-9]+$/) === null
        ? "Данные введены некорректно"
        : "Общая стоимость: " + c
    );
    return false;
}
